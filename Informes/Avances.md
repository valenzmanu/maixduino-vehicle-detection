# Avances

## Setup MAIXDUINO

Se investigó como conectar y configurar el MAIXDUINO. Casi no existe documentación para la placa y la poca que existe está regada en varios sitios de internet. Por esto, se comenzó a redactar la documentación para que cualquiera pueda seguirla y así evitar desperdiciar mucho tiempo en este paso en el futuro.

También se encontró que la placa puede programarse usando estos IDE's

+ Visual studio code (C, C++ o Python)
+ MaixPy (Python) - IDE desarrollado por los creadores de la placa

## Implementación funcional de YOLO

Para detectar y ubicar objetos en una imagen, la placa usa un algoritmo que se llama YOLO (You Only Look Once). YOLO fue inicialmente programado en C++, entonces se tuvo que buscar una implementación en Python. El script que se encontró permite utilizar un modelo de red neuronal pre-entrenado para detectar diferentes objetos en imágenes personalizadas.

*Sugerencia: Esto se podría ajustar para usar el feed de una cámara web y así probarlo en tiempo real*

## Entrenamiento de YOLOv3

Se encontró un script que dejaba usar imágenes de nuestra elección para re-entrenar a la red neuronal y así permitir que esta detecte carros. El re-entrenamiento necesitaba de dos cosas:

+ Imágenes que contenían carros
+ Anotaciones: Un archivo de texto que dice donde dentro de la imagen está el carro (En la imagen de abajo, la anotación diría cuales son las coordenadas y dimensiones de la cajita celeste)

![Validación](images/box.jpg)

Al conjunto de imágenes+anotaciones se le llama dataset. Se investigaron varios datasets y se escogió uno de vehículos de la universidad de Stanford. Las anotaciones tenían un formato no compatible entonces se debieron reformatear con un script personalizado.

El dataset de Stanford contiene casi 2000 imágenes y anotaciones, entonces se hizo una herramienta que extraía una muestra del dataset (Una pareja de imagen+anotación). Se extrajeron 300 muestras y se hicieron pruebas.

Se probó entrenar a la red utilizando el CPU y la GPU. El entrenamiento en CPU duró 18 horas, mientras que el entrenamiento con GPU duró 30 minutos y no trababa la computadora en el proceso.

El modelo final pesaba bastante y no dejaba de dar errores, entonces se tuvo que buscar otro script para el entrenamiento. El nuevo script ya traía un modelo que no solo pesaba menos de la mitad sino que ya detectaba carros con relativa precisión. Los resultados no eran perfectos, pero si lo suficientemente buenos para pasar al siguiente paso.

![Validación](images/Figure_1.png)

## Cargando el Modelo Entrenado a la Placa

El script de entrenamiento retorna el modelo de la red en formato `.h5`. Este lo puede usar una computadora y correr YOLO en tiempo real, pero el MAIXDUINO no lo va a correr con la velocidad que queremos. Entonces primero se convirtió a `.tflite`, un archivo optimizado para sistemas de bajos recursos.

El MAIXDUINO necesita un formato tipo `.kmodel`, entonces se usó un programa que dan los creadores de la placa para convertir a este formato.

La conversión fue exitosa, pero el modelo no quiso cargar en la placa. Supongo que por el tamaño del modelo. Se tienen que hacer más pruebas para ver si el error no viene del lado de nuestro script de entrenamiento. Por cualquier cosa, también se va a contactar a los creadores de la placa para resolver algunas dudas.
