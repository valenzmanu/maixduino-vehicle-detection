import cv2, time


# Create video object
video = cv2.VideoCapture(0)

while True:
    ret, img = video.read()

    cv2.imshow('Video', img)

    if (cv2.waitKey(10) & 0xFF == ord('b')):
        break
