import numpy as np
from numpy import expand_dims
from matplotlib import pyplot
from matplotlib.patches import Rectangle
from keras.models import load_model
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array

# -------------------------
# CLASES
# -------------------------

# A partir de la predicción generada se retornan las coordenadas y dimensiones de la
# boundary box de los objetos detectados.
class BoundBox:
    def __init__(self, xmin, ymin, xmax, ymax, objness = None, classes = None):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        
        self.objness = objness
        self.classes = classes

        self.label = -1
        self.score = -1

    def get_label(self):
        if self.label == -1:
            self.label = np.argmax(self.classes)
        
        return self.label
    
    def get_score(self):
        if self.score == -1:
            self.score = self.classes[self.get_label()]
            
        return self.score

# -------------------------
# FUNCIONES
# -------------------------

# Función sigmoide: Función monótona creciente que se encuentra acotada entre Y = 0
# y Y = 1. Por lo tanto, la función retorna valores decimales entre 0-1, cruzando X = 0 en
# la altura Y = 0.5.
def _sigmoid(x):
    return 1 / (1 + np.exp(-x))

# Decodifica la predicción realizada por el modelo
def decode_netout(netout, anchors, obj_thresh, net_h, net_w):
    grid_h, grid_w = netout.shape[:2]
    nb_box = 3
    netout = netout.reshape((grid_h, grid_w, nb_box, -1))
    nb_class = netout.shape[-1] - 5

    boxes = []

    netout[..., :2]  = _sigmoid(netout[..., :2])
    netout[..., 4:]  = _sigmoid(netout[..., 4:])
    netout[..., 5:]  = netout[..., 4][..., np.newaxis] * netout[..., 5:]
    netout[..., 5:] *= netout[..., 5:] > obj_thresh

    for i in range(grid_h*grid_w):
        row = i / grid_w
        col = i % grid_w
        
        for b in range(nb_box):
            # 4th element is objectness score
            objectness = netout[int(row)][int(col)][b][4]
            
            if(objectness.all() <= obj_thresh): continue
            
            # first 4 elements are x, y, w, and h
            x, y, w, h = netout[int(row)][int(col)][b][:4]

            x = (col + x) / grid_w # center position, unit: image width
            y = (row + y) / grid_h # center position, unit: image height
            w = anchors[2 * b + 0] * np.exp(w) / net_w # unit: image width
            h = anchors[2 * b + 1] * np.exp(h) / net_h # unit: image height  
            
            # last elements are class probabilities
            classes = netout[int(row)][col][b][5:]
            box = BoundBox(x-w/2, y-h/2, x+w/2, y+h/2, objectness, classes)
            boxes.append(box)

    return boxes

# Corrige el tamaño de las boundary boxes obtenidas estirándolas para hacerlas coincidir
# con las dimensiones originales de la imagen.
def correct_yolo_boxes(boxes, image_h, image_w, net_h, net_w):
	new_w, new_h = net_w, net_h
	for i in range(len(boxes)):
		x_offset, x_scale = (net_w - new_w)/2./net_w, float(new_w)/net_w
		y_offset, y_scale = (net_h - new_h)/2./net_h, float(new_h)/net_h
		boxes[i].xmin = int((boxes[i].xmin - x_offset) / x_scale * image_w)
		boxes[i].xmax = int((boxes[i].xmax - x_offset) / x_scale * image_w)
		boxes[i].ymin = int((boxes[i].ymin - y_offset) / y_scale * image_h)
		boxes[i].ymax = int((boxes[i].ymax - y_offset) / y_scale * image_h)

def _interval_overlap(interval_a, interval_b):
	x1, x2 = interval_a
	x3, x4 = interval_b
	if x3 < x1:
		if x4 < x1:
			return 0
		else:
			return min(x2,x4) - x1
	else:
		if x2 < x3:
			 return 0
		else:
			return min(x2,x4) - x3

# Calculates the "intersection over union of the boxes"
def bbox_iou(box1, box2):
	intersect_w = _interval_overlap([box1.xmin, box1.xmax], [box2.xmin, box2.xmax])
	intersect_h = _interval_overlap([box1.ymin, box1.ymax], [box2.ymin, box2.ymax])
	intersect = intersect_w * intersect_h
	w1, h1 = box1.xmax-box1.xmin, box1.ymax-box1.ymin
	w2, h2 = box2.xmax-box2.xmin, box2.ymax-box2.ymin
	union = w1*h1 + w2*h2 - intersect
	return float(intersect) / union

# Suprime las "non-maximal" boxes o filtra las detecciones de objetos al eliminar las
# boundary boxes redundantes
def do_nms(boxes, nms_thresh):
    if len(boxes) > 0:
        nb_class = len(boxes[0].classes)
    else:
        return
        
    for c in range(nb_class):
        sorted_indices = np.argsort([-box.classes[c] for box in boxes])

        for i in range(len(sorted_indices)):
            index_i = sorted_indices[i]

            if boxes[index_i].classes[c] == 0: continue

            for j in range(i+1, len(sorted_indices)):
                index_j = sorted_indices[j]

                if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_thresh:
                    boxes[index_j].classes[c] = 0

# Dibuja las boundary boxes (En conjunto con sus detalles) alrededor de los objetos detectados
# en la imagen original
def draw_boxes(frame, v_boxes, v_labels, v_scores):

    i = 0

    # Se dibujan las boundary boxes detectadas
    for box in v_boxes:
        y1, x1, y2, x2 = box.ymin, box.xmin, box.ymax, box.xmax                                     # Coordenadas de las dos esquinas opuestas del rectángulo
        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 255, 255), 1)                                # Se crea un rectángulo blanco con esquinas (x1, y1) y (x2, y2)
        label = "%s (%.3f)" % (v_labels[i], v_scores[i])                                            # Labels para los rectángulos en la forma: "Label (Probabilidad)"
        cv2.putText(frame, label, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))         # Se inicia la escritura de los labels en la coordenada (x1, y1), escalados a un 50% y con un 
        i += 1

# Carga los datos de los pixeles de una imagen
def normalize_image_data(image):

    # Mapear el valor original de los pixeles hacia un intervalo que va de 0 a 1.
    # Luego se mapea hacia un intervalo de 0 a 255
    image = image.astype('float32')
    image /= 255.0

    # Se agrega una dimensión adicional al array de "image" para contar con un único sample
    image = np.expand_dims(image, axis = 0)

    return image

# Se interpretan los resultados decodificados a partir del output de "predict()" 
def get_boxes(boxes, labels, thresh):
    v_boxes, v_labels, v_scores = list(), list(), list()

    # Se toma cada una de las "boxes" detectadas y decodificadas
    for box in boxes:

        # Se recorren todas las labels
        for i in range(len(labels)):

            # Se revisa si la label "i" tiene una probabilidad lo suficientemente alta
            if box.classes[i] > thresh:
                v_boxes.append(box)
                v_labels.append(labels[i])
                v_scores.append(box.classes[i]* 100)    # Multiplicado por 100 para convertir de decimal a porcentaje
    
    return v_boxes, v_labels, v_scores



# -------------------------
# PROGRAMA PRINCIPAL
# -------------------------

import cv2
video = cv2.VideoCapture(0)                                                                             # Se importa open-cv y se crea un objeto de video con la cámara en el slot 0

Mode = "Image"                                                                                         # Determinar modo: "Webcam" / "Image" / "Video"

Model = load_model('ModeloV1.h5')                                                                          # Cargar el modelo de la red neuronal YOLO3 (KERAS)
Input_Width, Input_Height = 416, 416                                                                    # Dimensiones requeridas por el modelo (Cambiar con respecto a las dimensiones de entrada colocadas en el modelo)


labels = ["person", "bicycle", "car", "motorbike", "aeroplane", "bus", "train", "truck",                # Posibles labels para los objetos detectados
    "boat", "traffic light", "fire hydrant", "stop sign", "parking meter", "bench",
    "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe",
    "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee", "skis", "snowboard",
    "sports ball", "kite", "baseball bat", "baseball glove", "skateboard", "surfboard",
    "tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon", "bowl", "banana",
    "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza", "donut", "cake",
    "chair", "sofa", "pottedplant", "bed", "diningtable", "toilet", "tvmonitor", "laptop", "mouse",
    "remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink", "refrigerator",
    "book", "clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush"]


while True:

    if Mode == "Image":
        MediaFolder = 'media'                                                           # MODIFICAR. Nombre del folder en el que se colocaron las fotografías o el video a analizar
        Filename = '0073.jpg'                                                           # MODIFICAR. Nombre del archivo que se desea analizar.
        FileDir = MediaFolder + '/' + Filename
        
        Frame = cv2.imread(FileDir)
        Width = Frame.shape[1]
        Height = Frame.shape[0]
        ImageData = load_img(FileDir, target_size = (Input_Width, Input_Height))        # Se redimensiona la imagen al tamaño requerido por la entrada de la red neuronal
        ImageData = img_to_array(ImageData)                                             # Convierte a un array de Numpy la información de la imagen

    elif Mode == "Webcam":
        Check, Frame = video.read()                                                     # Extraer una frame de video
        Width = Frame.shape[1]                                                          # Se obtienen las dimensiones originales de la imagen cargada
        Height = Frame.shape[0]

        ImageData = cv2.resize(Frame, (Input_Width, Input_Height))                      # Se redimensiona la imagen al tamaño requerido por la entrada de la red neuronal
    
    
    ImageData = normalize_image_data(ImageData)                                         # Se normalizan los valores de color RGB para que estén entre 0 y 255. Se agrega una cuarta dimensión al array de numpy
    Prediction = Model.predict(ImageData)                                               # Realizar una predicción
    
    print("Tarea: Prediccion - Completado")

    # Se definen las anclas de la imagen o "anchors"
    # ANCLA: Comúnmente se ha identificado que las boundary boxes que se detectan utilizando 
    # machine vision cuentan con "aspect ratios" similares entre sí. En YOLOv3, se utilizan 
    # 5 anclas y estas consisten de "offsets" con respecto a los "aspect ratios" estándar
    # propuestos por cada ancla
    anchors = [[116,90, 156,198, 373,326], [30,61, 62,45, 59,119], [10,13, 16,30, 33,23]]

    # Threshold de probabilidad para los objetos detectados
    # En otras palabras: Probabilidad de detección que debe existir para que una boundary box se considere válida
    class_threshold = 0.6

    # Se decodifica cada uno de los vectores obtenidos utilizando "decode_netout". Se guardan
    # las boundary boxes obtenidas en una lista inicialmente vacía.
    boxes = list()
    for i in range(len(Prediction)):
        boxes += decode_netout(Prediction[i][0], anchors[i], class_threshold, Input_Height, Input_Width)

    print("Tarea: Boundary Boxes - Completado")

    # Recordar que, para su análisis, la imagen fue redimensionada a un archivo de 416x416.
    # Por lo tanto, las boundary boxes se generaron sobre la imagen "comprimida". Si se desea
    # colocar las boundary boxes sobre la imagen original, se debe corregir el tamaño de las
    # mismas "estirándolas"
    correct_yolo_boxes(boxes, Height, Width, Input_Height, Input_Width)

    print("Tarea: Corrección Boundary Boxes - Completado")

    # Al decodificar los datos, se podrá observar que se generaron muchas boundary boxes muy
    # similares entre si, las cuales señalan al mismo objeto de manera redundante. Para evitar
    # repeticiones innecesarias, las boundary boxes se filtran "juntando" varias de estas
    # tomando en cuenta el "overlap" que existe entre las mismas. El threshold de overlap
    # se define en la forma de un porcentaje (50% en este caso). Esta etapa de post-procesado 
    # se le denomina "non-maximal suppression" o NMS
    do_nms(boxes, 0.5)

    print("Tarea: Non-maximal Suppression - Completado")

    # Se obtienen las propiedades asociadas a cada uno de los objetos detectados y encerrados
    # en una boundary box
    v_boxes, v_labels, v_scores = get_boxes(boxes, labels, class_threshold)

    print("Tarea: Get Boxes - Completado")

    # Se dibujan las boundary boxes finales
    #Frame = cv2.resize(Frame, (Input_Width, Input_Height))
    draw_boxes(Frame, v_boxes, v_labels, v_scores)
    cv2.imshow('Prediction', Frame)

    print("Tarea: Dibujar Cajas - Completado")

    if Mode == "Image":
        cv2.waitKey(0)
        break

    elif (Mode == "Webcam") and (cv2.waitKey(10) & 0xFF == ord('b')):
        break
