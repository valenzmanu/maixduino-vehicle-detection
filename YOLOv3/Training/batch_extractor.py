import os
import shutil

def delete_directory(path):
    print("Directory: ", path)

    if os.path.exists(path):                                                # If: Path given exists                        

        if len(os.listdir(path)) == 0:                                      # If the directory is empty, the program deletes the directory
            print("No further action needed. Directory already exists.")     
            
        else:                                                               # If the directory is not empty the user its asked for permission to delete the directory
            print("Directory is not empty:")
            option = input("Want to delete its contents? (y/n): ")

            if option == "y" or option == "Y":                              # Option selected is "yes"               
                try:
                    shutil.rmtree(path)                                     # Try to delete the directory and all of its contents
                except OSError as e:
                    print("Error: %s - %s." % (e.filename, e.strerror))     # Return an error if any pop-up
                    
                os.mkdir(path)                                              # Then create the desired directory
           
            else:
                print("Directory not deleted")
    
    else:
        print("Directory doesn't exist. It has been created.")
        os.mkdir(path)                                                      # If path doesn't exist, it is created.


ann_dir = "dataset/annotations"         # Directorio: Folder con anotaciones XML
image_dir = "dataset/images"            # Directorio: Folder con imágenes de entrenamiento

BatchSize = 5000                                                                    # Número de imágenes + anotaciones a extraer

# Program checks every element in the directory "image_dir"
# If said element is a file, it is concatenated at the end of a list
# len() then returns the lenght of the list. 
ImageNum = len([name for name in os.listdir(image_dir) if os.path.isfile(image_dir + "/" + name)])  

Step = round(ImageNum / (BatchSize + 1))                                            # Número de imágenes que se salta para extraer la cantidad deseada

print("")
print("Directory Cleaning")

target_image_dir = image_dir + "_batch"
target_ann_dir = ann_dir + "_batch"

delete_directory(target_image_dir)
delete_directory(target_ann_dir)

print("")
print("Batch Creation")

image_names = os.listdir(image_dir)
ann_names = os.listdir(ann_dir)
fileCount = 0

for i in range(0, ImageNum + 1, Step):
    
    shutil.copy(image_dir + "/" + image_names[i], target_image_dir + "/" + image_names[i])
    shutil.copy(ann_dir + "/" + ann_names[i], target_ann_dir + "/" + ann_names[i])

    fileCount += 1

    if fileCount % (BatchSize / 10) == 0:
        print("Progress: ", (fileCount/BatchSize) * 100, "%")

    if fileCount == BatchSize:
        break

print("Batch creation finished.")